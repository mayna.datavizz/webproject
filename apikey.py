import os

import uvicorn
from fastapi import FastAPI, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from starlette import status

# Use token based authentication
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


# Ensure the request is authenticated
def auth_request(token: str = Depends(oauth2_scheme)) -> bool:
    authenticated = token == os.getenv("API_KEY", "DUMMY-API-KEY")
    return authenticated


app = FastAPI()


@app.get("/openOrders")
async def open_orders(authenticated: bool = Depends(auth_request)):
    # Check for authentication like so
    if not authenticated:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not authenticated")

    # Business logic here
    return {"message": "Authentication Successful"}


if __name__ == '__main__':
    uvicorn.run("main:app", host="127.0.0.1", port=3000)
