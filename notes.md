## Test

Tasks:
- API Key based validation
- Username/Password based validation
- CSRF based validation

Links:
- https://fastapi.tiangolo.com/tutorial/security/
- https://www.pedaldrivenprogramming.com/2020/12/adding-real-user-authentication-to-fastapi/
- https://www.stackhawk.com/blog/csrf-protection-in-fastapi/
- https://www.youtube.com/watch?v=eWEgUcHPle0
- https://stackoverflow.com/questions/67942766/fastapi-api-key-as-parameter-secure-enough
- https://indominusbyte.github.io/fastapi-jwt-auth/usage/jwt-in-cookies/

## BASIC:

> curl http://localhost:8000/user

{"detail":"Missing Authorization Header"}

> curl -H "Content-Type: application/json" -X POST -d '{"username":"test","password":"test"}' http://localhost:8000/login

{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjAzNjkyMjYxLCJuYmYiOjE2MDM2OTIyNjEsImp0aSI6IjZiMjZkZTkwLThhMDYtNDEzMy04MzZiLWI5ODJkZmI3ZjNmZSIsImV4cCI6MTYwMzY5MzE2MSwidHlwZSI6ImFjY2VzcyIsImZyZXNoIjpmYWxzZX0.ro5JMHEVuGOq2YsENkZigSpqMf5cmmgPP8odZfxrzJA"}

> export TOKEN=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjAzNjkyMjYxLCJuYmYiOjE2MDM2OTIyNjEsImp0aSI6IjZiMjZkZTkwLThhMDYtNDEzMy04MzZiLWI5ODJkZmI3ZjNmZSIsImV4cCI6MTYwMzY5MzE2MSwidHlwZSI6ImFjY2VzcyIsImZyZXNoIjpmYWxzZX0.ro5JMHEVuGOq2YsENkZigSpqMf5cmmgPP8odZfxrzJA

> curl -H "Authorization: Bearer $TOKEN" http://localhost:8000/user

## ADD REFRESH TOKEN
Filename: basic.py

1. Login and get token.

> curl -H "Content-Type: application/json" -X POST -d '{"username":"test","password":"test"}' http://localhost:8000/login

{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjUzMzc2NDcyLCJuYmYiOjE2NTMzNzY0NzIsImp0aSI6Ijc2ZmE3NTY0LWQyMGMtNGYzNC1iZWMzLTRkODFiNWNlMWZjMyIsImV4cCI6MTY1MzM3NzM3MiwidHlwZSI6ImFjY2VzcyIsImZyZXNoIjpmYWxzZX0.YAwmKpoYaKFAKEIIoXtFJWK7ek5TnKMslTaOlBi45Q0","refresh_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjUzMzc2NDcyLCJuYmYiOjE2NTMzNzY0NzIsImp0aSI6IjU0NTg4MTg0LTE4MTgtNDI2ZC04MTc1LTdkMzc2ODg5YzM4MCIsImV4cCI6MTY1NTk2ODQ3MiwidHlwZSI6InJlZnJlc2gifQ.jjGhLeJaO2RTDb4-5d3xZXlnR75mtdI936PhwweM5PI"}

2. Export REFRESH Token.
> export TOKEN=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjUzMzc2NDcyLCJuYmYiOjE2NTMzNzY0NzIsImp0aSI6IjU0NTg4MTg0LTE4MTgtNDI2ZC04MTc1LTdkMzc2ODg5YzM4MCIsImV4cCI6MTY1NTk2ODQ3MiwidHlwZSI6InJlZnJlc2gifQ.jjGhLeJaO2RTDb4-5d3xZXlnR75mtdI936PhwweM5PI


3. Access /refresh endpoint.
> curl -H "Content-Type: application/json" -X POST -d '{"username":"test","password":"test"}' -H "Authorization: Bearer $TOKEN" http://localhost:8000/refresh

4. To check the user endpoint we need to login using access token again, that's mean we need to export the access token again.

> export TOKEN=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjUzMzc2NDcyLCJuYmYiOjE2NTMzNzY0NzIsImp0aSI6Ijc2ZmE3NTY0LWQyMGMtNGYzNC1iZWMzLTRkODFiNWNlMWZjMyIsImV4cCI6MTY1MzM3NzM3MiwidHlwZSI6ImFjY2VzcyIsImZyZXNoIjpmYWxzZX0.YAwmKpoYaKFAKEIIoXtFJWK7ek5TnKMslTaOlBi45Q0

5. Access /user endpoint.
> curl -H "Authorization: Bearer $TOKEN" http://localhost:8000/user

6. Test /partially-protected or /protected endpoint.

partially protected:
> curl -H "Authorization: Bearer $TOKEN" http://localhost:8000/partially-protected

{"user":"test"}

protected:
> curl -H "Authorization: Bearer $TOKEN" http://localhost:8000/protected

{"user":"test"}

## Revoking Token
Filename: main.py

Access Using Fresh Token:

> curl -H "Content-Type: application/json" -X POST -d '{"username":"test","password":"test"}' http://localhost:8000/fresh-login

{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjUzNDUwNzEzLCJuYmYiOjE2NTM0NTA3MTMsImp0aSI6IjU4YjBhNjI3LTViY2MtNDllYi1hM2NmLTc5NjU4ZDAwNzBjMyIsImV4cCI6MTY1MzQ1MTYxMywidHlwZSI6ImFjY2VzcyIsImZyZXNoIjp0cnVlfQ.M76su7LUOEktyoqAUbGDVzWImH-sNTeAMecg2xggeyA"}

> export TOKEN=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjUzNDUwNzEzLCJuYmYiOjE2NTM0NTA3MTMsImp0aSI6IjU4YjBhNjI3LTViY2MtNDllYi1hM2NmLTc5NjU4ZDAwNzBjMyIsImV4cCI6MTY1MzQ1MTYxMywidHlwZSI6ImFjY2VzcyIsImZyZXNoIjp0cnVlfQ.M76su7LUOEktyoqAUbGDVzWImH-sNTeAMecg2xggeyA

> curl -H "Authorization: Bearer $TOKEN" http://localhost:8000/protected-fresh

Revoke: (FAILED)
> curl -H "Content-Type: application/json" -X POST -d '{"username":"test","password":"test"}' -H "Authorization: Bearer $TOKEN" http://localhost:8000/refresh-revoke

## Cookie but Without Cookie (LOL)
Hi Prachi Goodluck! :)

1. Login
> curl -H "Content-Type: application/json" -X POST -d '{"username":"test","password":"test"}' http://localhost:8000/login

{"msg":"Successfully login","access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjUzNDU1NjYxLCJuYmYiOjE2NTM0NTU2NjEsImp0aSI6IjE2MTJjNmUwLTgwM2ItNGY5MC05YTU5LTU5NDUyYzFkYTFhOCIsImV4cCI6MTY1MzQ1NjU2MSwidHlwZSI6ImFjY2VzcyIsImZyZXNoIjpmYWxzZX0.M6jiKxvUXtcCKoRAYB12pKk7aXr5NNWpOzWPLoUNlRI","refresh_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjUzNDU1NjYxLCJuYmYiOjE2NTM0NTU2NjEsImp0aSI6ImYwOTc0ZGUyLTM3MzMtNDg1NS04YzNkLTBjM2JkMmRkNjE1YyIsImV4cCI6MTY1NjA0NzY2MSwidHlwZSI6InJlZnJlc2gifQ.QHZbIo_Z77DM_QgW1lGT1Jm5pjhJOYaTM94ArROtZYg"}

2. Export Refresh Token (copy from refresh_token)
> export TOKEN=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjUzNDU0OTE5LCJuYmYiOjE2NTM0NTQ5MTksImp0aSI6IjRjMGMwODliLTY4MTktNDU4Mi05YjAzLTQ1M2M2NjgwMDdjNiIsImV4cCI6MTY1NjA0NjkxOSwidHlwZSI6InJlZnJlc2gifQ.Bg3ZtcQ8rATv0ADY1TvoSU7H9X9Y7C2Muawrhs8-ajY

3. Access Refresh
> curl -H "Content-Type: application/json" -X POST -d '{"username":"test","password":"test"}' -H "Authorization: Bearer $TOKEN" http://localhost:8000/refresh

{"msg":"The token has been refresh","new_access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjUzNDU1MTE3LCJuYmYiOjE2NTM0NTUxMTcsImp0aSI6IjNlODQzYzk4LTRkZTUtNDAzOC1hODZkLTE0MTM4ZjZhZGM5MyIsImV4cCI6MTY1MzQ1NjAxNywidHlwZSI6ImFjY2VzcyIsImZyZXNoIjpmYWxzZX0.yyusAchqrZyS65WdRzAw85nwCvh1IIWbp6iluM2MjNE"}

Notes:
- If we want to check current user we neet to use access token, but we are exporting fresh token before, so this method is not allowed while using fresh token.
- If we want to check user and logout endpoint using fresh token it will only show:

{"detail":"Only access tokens are allowed"}

- So, export the access token again to access.

4. Check Current User
> curl -H "Authorization: Bearer $TOKEN" http://localhost:8000/protected

{"user":"test"}

5. Logout
p.s: I assume we fail because we use -X POST instead of -X DELETE hahahaha

> curl -H "Content-Type: application/json" -X DELETE -d '{"username":"test","password":"test"}' -H "Authorization: Bearer $TOKEN" http://localhost:8000/logout

{"msg":"Successfully logout"}


