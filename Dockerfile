# syntax=docker/dockerfile:1

FROM python:3.8.13-slim-bullseye 

WORKDIR /webproject

COPY ./requirements.txt /webproject/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /webproject/requirements.txt

COPY . /webproject

CMD ["python","-m","uvicorn", "main:app", "--host", "0.0.0.0" , "--port", "3000", "--reload"]
